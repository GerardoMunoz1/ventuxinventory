# Inventario Digitial Ventux
Primer proyecto de programación, unidad 2
Gerardo Muñoz & Benjamín Rojas

# Propuesta del problema:
Se pide la generación de una solución a alguna problemática que los mismos
estudiantes estimen conveniente, tomando en consideración el uso de interfaces
gráficas gracias al software Glade.

# Planteamiento del problema:
Se planteó el siguiente problema: generar un código incluyendo diferentes
interfaces gráficas para el ordenamiento de un inventario digital de una tienda
y poder agregar, eliminar y/o revisar el estado del inventario actual.

# Descripción de la solución:
Se logró generar una interfaz gráfica que permite al usuario, de una forma 
mucho más didáctica y animada, revisar el estado de un inventario digital de una
tienda en particular, ademáś de agregar o eliminar distintos productos como se 
estime conveniente.

# Objetivo de la solución:
-Lograr una interfaz gráfica mediante la utilización de Glade, para generar un
inventario digital que permita ver el estado, añadir o eliminar productos.

# Fallos del programa:
-El botón de 'Acerca de', no permite el acceso a la ventana para visualizar el
estado del programa, su versión y autores.
-Dentro de la administración del inventario, el programa no guarda de manera
existosa el producto y precio deseado, lo cual no permite la posterior elminación
si es que el usuario lo desea.