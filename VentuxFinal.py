import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import json
# Método para abrir archivos json
def open_file():
	try:
		with open('inventario.json', 'r') as file:
			data = json.load(file)
		file.close()
	except IOError:
		data = []

	return data
# Método para guardar archivos json
def save_file(data):
    print("Save File")

    # Guardamos todos los datos enviados en 'data' en formato Json
    with open('inventario.json', 'w') as file:
        json.dump(data, file, indent=4)
    file.close()
# Clase para ejecutar el menú inicial
class MenuInicial():

    def __init__(self):
        print("Constructor!")
        # Se crea un objeto para manipular Gtk
        self.builder = Gtk.Builder()
        # Se agregan objetos desde Ventux.glade
        self.builder.add_from_file("Ventux.glade")
        window = self.builder.get_object("menuInicial")
        window.connect("destroy", Gtk.main_quit)
        # Tamaño de la ventana inicial
        window.set_default_size(800, 400)
        # Titulo de la ventana inicial
        window.set_title("Ventux")

        # Botones
        # Boton para ingresar como administrador
        self.BotonAdmin = self.builder.get_object("botonIngresarAdmi")
        # Boton para revisar el 'Acerca de'
        self.BotonAcercade = self.builder.get_object("botonAcercaDe")
        # Boton para ingresar como cliente (proximamente)
        self.BotonCliente = self.builder.get_object("botonIngresarCliente")
        # Mostrar todos los componentes de la ventana
        window.show_all()
        # Activacion de los botones
        self.BotonAdmin.connect("clicked", self.BotonAdmi)
        self.BotonAcercade.connect("clicked", self.BotonAcercaDe)
        self.BotonCliente.connect("clicked", self.Boton_Cliente)
    # Método para ingresar como Administrador
    def BotonAdmi(self, btn=None):

    	aux = InventarioAdmin()
    	print("\nDecidiste ingresar como Administrador.")
    # Método para abrir 'Acerca de'
    def BotonAcercaDe(self, btn=None):

    	aux = About()
    	print("\nPresionaste el boton 'Acerca de'.")
    # Método para ingresar como cliente
    def Boton_Cliente(self, btn=None):

    	aux = ClienteProx()
    	print("\nProximamente...")
"""
# Clase para confirmar la contraseña de Administrador
class ConfirmarContraseñaAdmi():

	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("Ventux.glade")
		ventanacontra = self.builder.get_object("confirmarContraseñaAdmi")
		ventanacontra.set_default_size(500, 150)
		ventanacontra.set_title("Confirmar contraseña")
		
		self.BotonConfirmarContraseña = self.builder.get_object("botonConfirmarIngresoAdmi")
		self.BotonCancelarContraseña = self.builder.get_object("botonCancelarIngresoAdmi")
        # Contraseña establecida para ingresar
		ContraseñaParaIniciar = 1234

		# texto = self.entradaContraseña.get_text()

		# if (texto == ContraseñaParaIniciar):


		ventanacontra.show_all()
"""
# Clase para ejecutar la ventana 'Acerca de'
class About():
    # Se repite la inicialización
    # Se define builder, tamaño y nombre de la ventana
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("Ventux.glade")
		ventanaAbout = self.builder.get_object("ventanaAcercaDe")
		ventanaAbout.set_default_size(500, 400)
		ventanaAbout.set_title("VENTUX")

		ventanaAbout.show_all()
# Clase para ver panel de administración del inventario digital
class InventarioAdmin():
    # Se repite la inicialización
    # Se define builder, tamaño y nombre de la ventana
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("Ventux.glade")
		v_admin = self.builder.get_object("ventanaPrincipalAdmi")
		v_admin.set_default_size(600, 300)
		v_admin.set_title("Inventario Digital")

		self.BotonAgregarProducto = self.builder.get_object("botonAdd")
		self.BotonEliminarProducto = self.builder.get_object("botonElim")
		self.BotonRevisarInventario = self.builder.get_object("botonRevInv")

		v_admin.show_all()

		self.BotonAgregarProducto.connect("clicked", self.Añadir)
		self.BotonEliminarProducto.connect("clicked", self.Eliminar)
		self.BotonRevisarInventario.connect("clicked", self.RevisarInv)

	def Añadir(self, btn=None):

		auxiliar = Añadir_Producto()

	def Eliminar(self, btn=None):

		auxiliar = Eliminar_Producto()

	def RevisarInv(self, btn=None):

		auxiliar = Revisar_Inventario()
# Clase para ingresar como cliente (proximamente)
class ClienteProx():
    # Se repite la inicialización
    # Se define builder, tamaño y nombre de la ventana
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("Ventux.glade")
		prox = self.builder.get_object("clientee")
		prox.set_default_size(350, 100)
		prox.set_title("PRONTO")

		prox.show_all()
# Clase para añadir producto desde la administración
class Añadir_Producto():
    # Se repite la inicialización
    # Se define builder, tamaño y nombre de la ventana
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("Ventux.glade")
		añadir_p = self.builder.get_object("ventanaAgregarProducto")
		añadir_p.set_default_size(350, 200)
		añadir_p.set_title("Añadir un producto")
		self.aceptarañadir = self.builder.get_object("botonConfirmarAñadirProducto")
		añadir_p.show_all()
		self.aceptarañadir.connect("clicked", self.ListaProductosAñadir)
		
		self.produc = self.builder.get_object("entradaProductoAP")
		self.precio = self.builder.get_object("entradaPrecioAP")
		

	def ListaProductosAñadir(self, btn=None):
		lista1 = self.produc.get_text()
		lista2 = self.precio.get_text()
		codigo = 1

		j = {"Precio": lista2, "Producto": lista1}

		code = {str(codigo): []}
		
		code[str(codigo)].append(j)
		
		f = open_file()
		print(f)
		f.append(code)
		save_file(f)

		#self.ventanaAgregarProducto.destroy()

	print("Se ha registrado correctamente el producto.")

		# print(inventario)
# Clase para eliminar producto desde la administración
class Eliminar_Producto():
    # Se repite la inicialización
    # Se define builder, tamaño y nombre de la ventana
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("Ventux.glade")
		eliminar_p = self.builder.get_object("ventanaEliminarProducto")
		eliminar_p.set_default_size(350, 200)
		eliminar_p.set_title("Eliminar un producto")

		eliminar_p.show_all()
# Clase para revisar el inventario general
class Revisar_Inventario():
    # Se repite la inicialización
    # Se define builder, tamaño y nombre de la ventana
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("Ventux.glade")
		rev_inv = self.builder.get_object("ventanaRevisarInventario")
		rev_inv.set_default_size(350, 200)
		rev_inv.set_title("Revisar Inventario")

		self.listmodel = Gtk.ListStore(str, str)
		self.treeResultado = self.builder.get_object("treeResultado")
		self.treeResultado.set_model(model=self.listmodel)

		cell = Gtk.CellRendererText()
		title = ("Producto", "Precio")
		for i in range(len(title)):
			col = Gtk.TreeViewColumn(title[i], cell, text=i)
			self.treeResultado.append_column(col)

		self.show_all_data()

		rev_inv.show_all()

	def show_all_data(self):

		data = open_file()

		print (data)

		for i in range (len(data)):
			x = data[i][str(1)][0].values()
			self.listmodel.append(x)

if __name__ == '__main__':
    MenuInicial()
    Gtk.main()